from task_1.txtWorker import TxtWorker
import sys

sys.path.append('/Users/ilsat/myFiles/itis/data_mining/db_kr')


def get_buckets():
    filename = 'transactions.csv'
    files = TxtWorker.read(filename)
    files = files.split('\n')[1:]
    files = [file.split(';') for file in files]
    print(files)
    buckets = {}
    all_products = {}
    for file in files:
        prod = file[0]
        buck = file[1]
        elems = buckets.get(buck, [])
        elems.append(prod)
        buckets[buck] = elems

        num = all_products.get(prod, 0) + 1
        all_products[prod] = num

    return buckets, all_products


def regroup_bucket(bucket):
    """ каждый с каждым """
    new_buck = []
    n = 1
    for i in bucket:
        for k in range(n, len(bucket)):
            new_buck.append([bucket[i], bucket[k]])
        n += 1


def del_bad_products(products_dict, min_num=2):
    """ min_num = уровень поддержки"""
    new_prods = {}
    for prod, num in products_dict.items():
        if num < min_num:
            pass
        else:
            new_prods[prod] = num

    return new_prods


def del_bad_products_in_buckets(buckets, good_prods_dict):
    new_buckets = {}
    good_prods = set(good_prods_dict.keys())
    for key, value in buckets.items():
        new_l = []
        for elem in value:
            if elem in good_prods:
                new_l.append(elem)
        if len(new_l) != 0:
            new_buckets[key] = new_l

    return new_buckets





buckets,  all_products = get_buckets()
all_products = del_bad_products(all_products, 2)
buckets = del_bad_products_in_buckets(buckets, all_products)

# профильтровали на уровень поддержки


