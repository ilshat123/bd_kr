from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


from task_1.test import Base


class ConnectBd:
    engine = create_engine('postgresql://test_user:test_password@localhost:5432/test_database')

    def __init__(self):
        self.create_table()
        session = sessionmaker(bind=self.engine)
        self.s = session()

    def create_table(self):
        Base.metadata.create_all(self.engine)


bd = ConnectBd()
