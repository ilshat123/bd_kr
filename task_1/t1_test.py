import numpy as np

l = [
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 1],
    [0, 0, 0, 0, 0, 0],

]

l_t = [
    [0, 0, 0, 0, 0, 0],
    [1, 0, 0, 1, 0, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0],
]

h = [1, 1, 1, 1, 1, 1]


def calculate_vector(h, l, l_t):
    l_t_arr = np.array(l_t)
    l_t__h = l_t_arr.dot(np.array(h))
    lamd = max(l_t__h)
    a = [elem/lamd for elem in l_t__h]
    l_a = np.array(l).dot(np.array(a))
    nu = max(l_a)
    new_h = [elem/nu for elem in l_a]
    return new_h, a


def calculate_auth(h, l, l_t, iterations):
    hubbiness, auth = None, None
    for i in range(iterations):
        hubbiness, auth = calculate_vector(h, l, l_t)
        h = hubbiness
        # print(new_h)
        # input()
    return hubbiness, auth


if __name__ == '__main__':
    hub, auth = calculate_auth(h, l, l_t, 3)
    print('hubbiness: ', hub)
    print('authority:', auth)