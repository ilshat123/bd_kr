import math
import random
import re
import time
import sys

sys.path.append('/Users/ilsat/myFiles/itis/data_mining/db_kr')

from task_1.txtWorker import TxtWorker


class CountableBloomFilter:
    def __init__(self, num_elems, false_positive):
        self.n = num_elems
        self.p = false_positive
        self.m = int(- (self.n * math.log(self.p, math.exp(1))) / (math.log(2, math.exp(1)) ** 2))
        self.k = int((self.m / self.n) * math.log(2, math.exp(1)))

        if self.k == 0:
            self.k = 1

        print(self.m)
        print(self.k)

        self.arr = [0 for i in range(self.m)]
        self.hash_funcs = self.create_hash_funcs(self.k)

    def create_hash_funcs(self, k):
        funcs = []
        for i in range(k):
            funcs.append(self._get_hash_func(self.m))
        return funcs

    def _get_hash_func(self, arr_size):
        k = random.randrange(arr_size * arr_size)

        def func(str_element):
            num = 0
            f_k = k
            for i in str_element:
                num += ord(i)
            return (num + f_k) % arr_size
        return func

    def add_element(self, str_line):
        for func in self.hash_funcs:
            index = func(str_line)
            # print(index)
            self.arr[index] += 1


if __name__ == '__main__':
    filename = "2_task_text.txt"
    text = TxtWorker.read(filename)
    words = re.findall(r'\w+', text)
    words = [word.lower() for word in words]

    false_positive = 0.1

    b_filter = CountableBloomFilter(len(words), false_positive)

    for word in words:
        b_filter.add_element(word)
    print(words)
    print(len(words))
    print(b_filter.arr)


# print(ord('f'))


# print(time.time)

# obj = CountableBloomFilter(100, 0.1)
#
#
# num = math.log1p(2)
#
# print(num)
#